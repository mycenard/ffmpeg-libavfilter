#include <stdio.h>
#include "libavfilter/buffersink.h"
#include "libavfilter/buffersrc.h"
#include "libavfilter/formats.h"
#include "libavutil/opt.h"
#include "libavutil/pixdesc.h"
#include "libavutil/pixfmt.h"
#include "libavutil/imgutils.h"
#include "libavutil/avassert.h"
#include "avfilter.h"
#include "formats.h"
#include "internal.h"
#include "video.h"


typedef struct MyFilterContext {
    const AVClass *class;
    // Add your filter-specific fields here if needed
} MyFilterContext;

static int query_formats(AVFilterContext *ctx) {
    static const enum AVPixelFormat pix_fmts[] = { AV_PIX_FMT_YUV420P, AV_PIX_FMT_NONE };
    AVFilterFormats *formats = ff_make_format_list(pix_fmts);
    if (!formats)
        return AVERROR(ENOMEM);

    return ff_set_common_formats(ctx, formats);
}

static int config_input(AVFilterLink *inlink) {
    // Configure input properties here if needed
    return 0;
}

static int config_output(AVFilterLink *outlink) {
    // Configure output properties here if needed
    return 0;
}

static int filter_frame(AVFilterLink *inlink, AVFrame *frame) {
    // Process each frame here
    printf("success\n");

    return ff_filter_frame(inlink->dst->outputs[0], frame);
}

static const AVFilterPad avfilter_vf_myfilter_inputs[] = {
    {
        .name = "default",
        .type = AVMEDIA_TYPE_VIDEO,
        .filter_frame = filter_frame,
        .config_props = config_input,
    },
    { NULL }
};

static const AVFilterPad avfilter_vf_myfilter_outputs[] = {
    {
        .name = "default",
        .type = AVMEDIA_TYPE_VIDEO,
        .config_props = config_output,
    },
    { NULL }
};

AVFilter ff_vf_transform = {
    .name = "transform",
    .description = NULL_IF_CONFIG_SMALL("A filter that prints 'success' on each frame."),
    .inputs = avfilter_vf_myfilter_inputs,
    .outputs = avfilter_vf_myfilter_outputs,
    .priv_class = NULL,
    .flags = 0,
    .nb_inputs = 1,
    .nb_outputs = 1,
    .formats_state = 0, // Set the correct formats state here
    .formats.query_func = query_formats,
    .priv_size = sizeof(MyFilterContext),
};
